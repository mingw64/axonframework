package com.example.demo;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventhandling.EventHandler;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

public class ToDoItem  {
    @AggregateIdentifier
    private String id;

    @CommandHandler
    public ToDoItem(CreateToDoItemCommand command){
        apply(new ToDoItemCreatedEvent(command.getTodoId(), command.getDescription()));
    }
    @EventHandler
    public void on(ToDoItemCreatedEvent event){
        this.id = event.getTodoId();
    }
}
