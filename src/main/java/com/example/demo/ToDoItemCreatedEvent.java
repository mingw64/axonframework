package com.example.demo;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


public class ToDoItemCreatedEvent {

    private  String todoId;
    private  String description;



    public ToDoItemCreatedEvent(String todoId, String description) {
        this.todoId = todoId;
        this.description = description;


    }

    public String getTodoId() {
        return todoId;
    }

    public String getDescription() {
        return description;
    }





}