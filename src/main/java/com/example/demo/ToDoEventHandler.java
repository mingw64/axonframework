package com.example.demo;

import org.axonframework.eventhandling.EventHandler;

public class ToDoEventHandler {
    @EventHandler
    public void handle(ToDoItemCreatedEvent event){
        System.out.println("We've got something to do: " + event.getDescription() + " (" + event.getTodoId() + ")");
    }

    @EventHandler
    public void handle(ToDoItemCompletedEvent event){
        System.out.println("We've comleted a task: " + event.getTodoId());
    }
}
